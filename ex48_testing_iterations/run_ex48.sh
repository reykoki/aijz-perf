#!/bin/bash

echo "EXAMPLE 48 - 3D FE" >> results_ex48
echo " " >> results_ex48

for i in {500..10000..500}
do

    for j in {1..10..1}
    do
        mpiexec -n 4 ../../petsc/arch-aijz-opt/tests/snes/tutorials/ex48 -pc_type none -dm_mat_type mpiaijz -ksp_max_it 100 -snes_max_it 3 -da_grid_x 500 -da_grid_y 500 -log_view -snes_view -mat_aijz_max_block_size $i > results/ex48_mpiaijz_${i}_iter_${j}

        mpiexec -n 4 ../../petsc/arch-aijz-opt/tests/snes/tutorials/ex48 -pc_type none -dm_mat_type mpiaij -mat_no_inode -ksp_max_it 100 -snes_max_it 3 -da_grid_x 500 -da_grid_y 500 -log_view -snes_view > results/ex48_mpiaij_${i}_iter_${j}
    done

done


