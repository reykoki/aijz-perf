import numpy as np
import re
import pandas as pd
import matplotlib.pyplot as plt
import os
from glob import iglob
import itertools


def sorted_nicely( l ):
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    return sorted(l, key = alphanum_key)

def plot_data(df, data_type, units):
    plt.figure(figsize=(8, 6))
    for label, data in df.groupby('label'):
        plt.plot(data['max_block_size'], data[data_type], label=label)
    plt.xlabel('max block size')
    plt.ylabel("%s [%s]" % (data_type, units))
    plt.xticks(data['max_block_size'][::2])
    plt.legend()
    plt.savefig('{}'.format(data_type))
    plt.title('noether n=60')
    plt.show()

def load_data():
    df = pd.read_pickle('noether_60.pickle')
    return df

df = load_data()
plot_data(df, 'bandwidth', 'MBHz')
#plot_data(df, 'runtime', 's')

