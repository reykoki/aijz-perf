#!/bin/bash

echo "EXAMPLE 77"

for i in {500..10000..500}
do

    mpiexec -bind-to core -n 64 ../../petsc/arch-aijz-opt/tests/snes/tutorials/ex77 -dm_refine 7 -bc_fixed 1 -bc_pressure 2 -wall_pressure 0.4 -log_view -dm_mat_type mpiaijz -dm_distribute -snes_view -dm_plex_box_faces 10,10,10 -mat_aij_max_block_size $i > results/ex77_mpiaijz_$i

    mpiexec -bind-to core -n 64 ../../petsc/arch-aijz-opt/tests/snes/tutorials/ex77 -dm_refine 7 -bc_fixed 1 -bc_pressure 2 -wall_pressure 0.4 -log_view -dm_mat_type mpiaij -dm_distribute -snes_view -dm_plex_box_faces 10,10,10 -mat_aij_max_block_size $i > results/ex77_mpiaij_$i

done


