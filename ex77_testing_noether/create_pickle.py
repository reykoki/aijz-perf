import numpy as np
import pickle
import re
import pandas as pd
import matplotlib.pyplot as plt
import os
from glob import iglob
import itertools


def sorted_nicely( l ):
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    return sorted(l, key = alphanum_key)


rootdir_glob = './results/*'
file_list = [f for f in iglob(rootdir_glob, recursive=True) if os.path.isfile(f)]
file_list = sorted_nicely(file_list)

def load_data():
    data = []
    for fn in file_list:
        file = open(fn)
        content = file.readlines()
        flops = float(content[0].split()[-1])*6
        runtime = content[0].split()[3]
        max_block_size = fn.split('_')[-1]
        if 'mpiaijz' in fn:
            label = 'mpiaijz'
        else:
            label = 'mpiaij'
        data.append([label, max_block_size, runtime, flops])
    df = pd.DataFrame(data, columns=['label', 'max_block_size', 'runtime', 'bandwidth'])
    return df

df = load_data()
df.to_pickle('noether_60.pickle')

